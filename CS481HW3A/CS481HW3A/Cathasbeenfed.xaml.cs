﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481HW3A
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Cathasbeenfed : ContentPage
	{
		public Cathasbeenfed (string catFedMessage)
		{
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            catFedLabel.Text = catFedMessage; //Gets message

        }

        async void OnGoBack(object sender, EventArgs e)
        {
            await Navigation.PopAsync();

        }
	}
}