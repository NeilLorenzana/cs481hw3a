﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481HW3A
{
    public partial class HomePage : ContentPage
    {
        public HomePage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this,false);
        }

        async void OnClickMe(object sender, EventArgs e) //Go to next Page
        {
            await Navigation.PushAsync(new Cathasbeenfed("The cat was fed!"));
            
        }

    }
}
